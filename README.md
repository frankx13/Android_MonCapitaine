# MonCapitaine

MonCapitaine is an application targetting people wanting to learn the basics of sea navigation. It contains the rudiments of the navigation in sea knowledge, as well as a glossary of maritim words and expressions.

The app contains two main components :

-The first component of the app is the theory, where one can acquire the principles of sailboats, for example.

-The second component is the practice, where different manoeuvers with a sailboat can be realized, it will help the navigator to get the good reflexes and provide him the tips to drive like a pro.

The application is written in Kotlin and target the Android users.
