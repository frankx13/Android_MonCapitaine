package com.studio.neopanda.moncapitaine

class Expression {
    var name: String = ""
    var explanation: String = ""
    var id: Int = 0

    constructor(name: String, explanation: String, id: Int) {
        this.name = name
        this.explanation = explanation
        this.id = id
    }
}