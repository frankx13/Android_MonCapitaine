package com.studio.neopanda.moncapitaine

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import butterknife.ButterKnife

class MainActivity : AppCompatActivity() {

    @BindView(R.id.btn_section_one)
    lateinit var sectionOne: Button
    @BindView(R.id.btn_section_two)
    lateinit var sectionTwo: Button
    @BindView(R.id.btn_section_three)
    lateinit var sectionThree: Button
    @BindView(R.id.btn_section_four)
    lateinit var sectionFour: Button
    @BindView(R.id.btn_section_five)
    lateinit var sectionFive: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ButterKnife.bind(this)

        sectionOne.setOnClickListener {
            val intent = Intent(this, SectionOneActivity::class.java)
            startActivity(intent)
        }
        sectionTwo.setOnClickListener {
            val intent = Intent(this, SectionTwoActivity::class.java)
            startActivity(intent)
        }
        sectionThree.setOnClickListener {
            val intent = Intent(this, SectionThreeActivity::class.java)
            startActivity(intent)
        }
        sectionFour.setOnClickListener {
            val intent = Intent(applicationContext, SaltySeaDogGlossaryActivity::class.java)
            startActivity(intent)
        }
        sectionFive.setOnClickListener {
            val intent = Intent(applicationContext, SectionFiveActivity::class.java)
            startActivity(intent)
        }
    }
}
