package com.studio.neopanda.moncapitaine

import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PartEightPrioritiesActivity : AppCompatActivity() {

    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null

    val data: SortedMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val whatIsPriorities = ArrayList<String>()
            whatIsPriorities.add(getString(R.string.partEightWhatIsPriorities))

            val generalRules = ArrayList<String>()
            generalRules.add(
                getString(R.string.partEightGeneralRules_a) +
                        "\n\n" + getString(R.string.partEightGeneralRules_b) +
                        "\n" + getString(R.string.partEightGeneralRules_c) +
                        "\n" + getString(R.string.partEightGeneralRules_d) +
                        "\n" + getString(R.string.partEightGeneralRules_e) +
                        "\n" + getString(R.string.partEightGeneralRules_f)
            )

            val betweenTwoMotorProps = ArrayList<String>()
            betweenTwoMotorProps.add(
                getString(R.string.partEightBtTwoMotorProp_a) + "\n" +
                        getString(R.string.partEightBtTwoMotorProp_b) + "\n" +
                        getString(R.string.partEightBtTwoMotorProp_c)
            )
            betweenTwoMotorProps.add(
                getString(R.string.partEightBtTwoMotorProp_d) + "\n" +
                        getString(R.string.partEightBtTwoMotorProp_e)
            )

            val betweenTwoVelicProps = ArrayList<String>()
            betweenTwoVelicProps.add(
                getString(R.string.partEightBtTwoVelicProp_a) + "\n" +
                        getString(R.string.partEightBtTwoVelicProp_b)
            )

            betweenTwoVelicProps.add(
                getString(R.string.partEightBtTwoVelicProp_c) + "\n" +
                        getString(R.string.partEightBtTwoVelicProp_d)
            )

            val betweenMotorAndVelicProps = ArrayList<String>()
            betweenMotorAndVelicProps.add(
                getString(R.string.partEightBtMotorVelicProp_a) + "\n\n" +
                        getString(R.string.partEightBtMotorVelicProp_b) + "\n\n" +
                        getString(R.string.partEightBtMotorVelicProp_c) + "\n" +
                        getString(R.string.partEightBtMotorVelicProp_d) + "\n" +
                        getString(R.string.partEightBtMotorVelicProp_e) + "\n" +
                        getString(R.string.partEightBtMotorVelicProp_f)
            )

            val duringDepassing = ArrayList<String>()
            duringDepassing.add(
                getString(R.string.partEightDuringDepassment_a) + "\n" +
                        getString(R.string.partEightDuringDepassment_b) + "\n" +
                        getString(R.string.partEightDuringDepassment_c)
            )

            val insideSmallWay = ArrayList<String>()
            insideSmallWay.add(
                getString(R.string.partEightSmallCanal_a) + "\n\n" +
                        getString(R.string.partEightSmallCanal_b) + "\n\n" +
                        getString(R.string.partEightSmallCanal_c)
            )


            listData[getString(R.string.partEightSubA)] = whatIsPriorities
            listData[getString(R.string.partEightSubB)] = generalRules
            listData[getString(R.string.partEightSubC)] = betweenTwoMotorProps
            listData[getString(R.string.partEightSubD)] = betweenTwoVelicProps
            listData[getString(R.string.partEightSubE)] = betweenMotorAndVelicProps
            listData[getString(R.string.partEightSubF)] = duringDepassing
            listData[getString(R.string.partEightSubG)] = insideSmallWay

            return listData.toSortedMap()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.part_one_activity)

        setUpActionBar()

        expandableListView = findViewById(R.id.expandableListView)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = CustomExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)
        }
    }

    fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.priorities)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
