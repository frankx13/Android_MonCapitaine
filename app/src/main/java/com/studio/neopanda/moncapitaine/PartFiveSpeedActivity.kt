package com.studio.neopanda.moncapitaine

import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PartFiveSpeedActivity : AppCompatActivity() {

    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null

    val data: SortedMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val speedKnot = ArrayList<String>()
            speedKnot.add(
                getString(R.string.partFiveSpeedKnot_a) +
                        "\n" + getString(R.string.partFiveSpeedKnot_b)
            )

            val calculateSpeed = ArrayList<String>()
            calculateSpeed.add(
                getString(R.string.partFiveCalculateSpeedKnot_a) +
                        "\n\n" + getString(R.string.partFiveCalculateSpeedKnot_b) +
                        "\n" + getString(R.string.partFiveCalculateSpeedKnot_c) +
                        "\n\n" + getString(R.string.partFiveCalculateSpeedKnot_d) +
                        "\n\n" + getString(R.string.partFiveCalculateSpeedKnot_e)
            )

            listData[getString(R.string.partFiveSubA)] = speedKnot
            listData[getString(R.string.partFiveSubB)] = calculateSpeed

            return listData.toSortedMap()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.part_one_activity)

        setUpActionBar()

        expandableListView = findViewById(R.id.expandableListView)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = CustomExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)
        }
    }

    fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.knots)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}