package com.studio.neopanda.moncapitaine

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PartFourFloatActivity : AppCompatActivity() {

    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null

    val data: SortedMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val floatabilityWhatIs = ArrayList<String>()
            floatabilityWhatIs.add(getString(R.string.partFourWhatIsFloatability))

            val floatabilityBasics = ArrayList<String>()
            floatabilityBasics.add(getString(R.string.partFourFloatabilityBasics))

            val principleOfArchimedes = ArrayList<String>()
            principleOfArchimedes.add(getString(R.string.partFourArchimedes_a) +
                    "\n" +  getString(R.string.partFourArchimedes_b))

            listData[getString(R.string.partFourSubA)] = floatabilityWhatIs
            listData[getString(R.string.partFourSubB)] = floatabilityBasics
            listData[getString(R.string.partFourSubC)] = principleOfArchimedes

            return listData.toSortedMap()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.part_one_activity)

        setUpActionBar()

        expandableListView = findViewById(R.id.expandableListView)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = CustomExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)
        }
    }

    fun setUpActionBar(){
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.floatability)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
