package com.studio.neopanda.moncapitaine

import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PartNineMeteoActivity : AppCompatActivity() {
    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null

    val data: SortedMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val beforeSailing = ArrayList<String>()
            beforeSailing.add(
                getString(R.string.partNineBeforeSailing_a) +
                        "\n" + getString(R.string.partNineBeforeSailing_b) +
                        "\n" + getString(R.string.partNineBeforeSailing_c)
            )

            val meteoDegradation = ArrayList<String>()
            meteoDegradation.add(
                getString(R.string.partNineMeteoDegradation_a) +
                        "\n\n" + getString(R.string.partNineMeteoDegradation_b) +
                        "\n" + getString(R.string.partNineMeteoDegradation_c) +
                        "\n" + getString(R.string.partNineMeteoDegradation_d) +
                        "\n" + getString(R.string.partNineMeteoDegradation_e)
            )

            val scaleOfBeaufort = ArrayList<String>()
            scaleOfBeaufort.add(
                getString(R.string.partNineBeaufortScale_a) +
                        "\n" + getString(R.string.partNineBeaufortScale_b)
            )

            val conceptionCategory = ArrayList<String>()
            conceptionCategory.add(
                getString(R.string.partNineConceptionCategory_a) +
                        "\n" + getString(R.string.partNineConceptionCategory_b) +
                        "\n\n" + getString(R.string.partNineConceptionCategory_c) +
                        "\n" + getString(R.string.partNineConceptionCategory_d) +
                        "\n" + getString(R.string.partNineConceptionCategory_e) +
                        "\n" + getString(R.string.partNineConceptionCategory_f)
            )
            conceptionCategory.add(getString(R.string.partNineConceptionCategory_g))

            listData[getString(R.string.partNineSubA)] = beforeSailing
            listData[getString(R.string.partNineSubB)] = meteoDegradation
            listData[getString(R.string.partNineSubC)] = scaleOfBeaufort
            listData[getString(R.string.partNineSubD)] = conceptionCategory

            return listData.toSortedMap()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.part_one_activity)

        setUpActionBar()

        expandableListView = findViewById(R.id.expandableListView)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = CustomExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)
        }
    }

    fun setUpActionBar(){
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.meteorology)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
