package com.studio.neopanda.moncapitaine

import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PartOneSectionTwoActivity : AppCompatActivity() {

    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null

    val data: SortedMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val barSystem = ArrayList<String>()
            barSystem.add(
                getString(R.string.partOneBarSystem_a) +
                        "\n" + getString(R.string.partOneBarSystem_b) +
                        "\n" + getString(R.string.partOneBarSystem_c)
            )

            val rubberComposition = ArrayList<String>()
            rubberComposition.add(
                getString(R.string.partOneRubberComposition_a) +
                        "\n\n" + getString(R.string.partOneRubberComposition_b) +
                        "\n" + getString(R.string.partOneRubberComposition_c) +
                        "\n" + getString(R.string.partOneRubberComposition_d)
            )

            val barFunctionnement = ArrayList<String>()
            barFunctionnement.add(
                getString(R.string.partOneBarFunctionnement_a)
            )

            listData[getString(R.string.partOneSectionTwoA)] = barSystem
            listData[getString(R.string.partOneSectionTwoB)] = rubberComposition
            listData[getString(R.string.partOneSectionTwoC)] = barFunctionnement

            return listData.toSortedMap()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.part_one_activity)

        setUpActionBar()

        expandableListView = findViewById(R.id.expandableListView)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = CustomExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)
        }
    }

    fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.manoeuver_its_boat)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
