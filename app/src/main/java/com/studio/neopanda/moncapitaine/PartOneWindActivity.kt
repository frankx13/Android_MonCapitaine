package com.studio.neopanda.moncapitaine

import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PartOneWindActivity : AppCompatActivity() {

    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null

    val data: SortedMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val windWhatIs = ArrayList<String>()
            windWhatIs.add(getString(R.string.partOneWhatIswWind))

            val howDoesItWork = ArrayList<String>()
            howDoesItWork.add(getString(R.string.partOnehowDoesItWork))

            val windSorts = ArrayList<String>()
            windSorts.add(getString(R.string.partOneWindSortsA))
            windSorts.add(getString(R.string.partOneWindSortsB))

            val windMaritim = ArrayList<String>()
            windMaritim.add(getString(R.string.partOneWindMaritim))

            listData[getString(R.string.partOneSubA)] = windWhatIs
            listData[getString(R.string.partOneSubB)] = howDoesItWork
            listData[getString(R.string.partOneSubC)] = windSorts
            listData[getString(R.string.partOneSubD)] = windMaritim

            return listData.toSortedMap()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.part_one_activity)
        setUpActionBar()

        expandableListView = findViewById(R.id.expandableListView)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = CustomExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)
        }
    }

    fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.the_wind)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
