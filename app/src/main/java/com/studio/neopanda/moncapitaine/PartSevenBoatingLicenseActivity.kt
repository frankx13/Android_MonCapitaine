package com.studio.neopanda.moncapitaine

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PartSevenBoatingLicenseActivity : AppCompatActivity() {

    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null

    val data: SortedMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val boatingLicenseWhatIs = ArrayList<String>()
            boatingLicenseWhatIs.add(getString(R.string.partSevenWhatIsBoatingLicense_a) +
                    "\n" + getString(R.string.partSevenWhatIsBoatingLicense_b))

            val howToGetIt = ArrayList<String>()
            howToGetIt.add(getString(R.string.partSevenHowToGetIt_a) +
                    "\n" + getString(R.string.partSevenHowToGetIt_b))
            howToGetIt.add(getString(R.string.partSevenHowToGetIt_c) +
                    "\n" + getString(R.string.partSevenHowToGetIt_d) +
                    "\n" + getString(R.string.partSevenHowToGetIt_e) +
                    "\n" + getString(R.string.partSevenHowToGetIt_f))
            howToGetIt.add(getString(R.string.partSevenHowToGetIt_g) +
                    "\n" + getString(R.string.partSevenHowToGetIt_h) +
                    "\n" + getString(R.string.partSevenHowToGetIt_i))

            val avantagesOfLicense = ArrayList<String>()
            avantagesOfLicense.add(getString(R.string.partSevenAvantagesOfLicense_a) +
                    "\n\n" + getString(R.string.partSevenAvantagesOfLicense_b) +
                    "\n" + getString(R.string.partSevenAvantagesOfLicense_c) +
                    "\n" + getString(R.string.partSevenAvantagesOfLicense_d) +
                    "\n" + getString(R.string.partSevenAvantagesOfLicense_e))

            val hauturiereExtension = ArrayList<String>()
            hauturiereExtension.add(getString(R.string.partSevenHauturiereExtension_a) +
                    "\n" + getString(R.string.partSevenHauturiereExtension_b))

            val sailboatException = ArrayList<String>()
            sailboatException.add(getString(R.string.partSevenSailboatException_a) +
                    "\n\n" + getString(R.string.partSevenSailboatException_b) +
                    "\n\n" + getString(R.string.partSevenSailboatException_c))

            listData[getString(R.string.partSevenSubA)] = boatingLicenseWhatIs
            listData[getString(R.string.partSevenSubB)] = howToGetIt
            listData[getString(R.string.partSevenSubC)] = avantagesOfLicense
            listData[getString(R.string.partSevenSubD)] = hauturiereExtension
            listData[getString(R.string.partSevenSubE)] = sailboatException

            return listData.toSortedMap()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.part_one_activity)

        setUpActionBar()

        expandableListView = findViewById(R.id.expandableListView)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = CustomExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)
        }
    }

    fun setUpActionBar(){
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.the_boat_license)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
