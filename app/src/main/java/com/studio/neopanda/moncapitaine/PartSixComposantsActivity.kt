package com.studio.neopanda.moncapitaine

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PartSixComposantsActivity : AppCompatActivity() {

    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null

    val data: SortedMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val floater = ArrayList<String>()
            floater.add(getString(R.string.partSixFloater_a) +
                    "\n\n" + getString(R.string.partSixFloater_b) +
                    "\n" + getString(R.string.partSixFloater_c) +
                    "\n" + getString(R.string.partSixFloater_d) +
                    "\n" + getString(R.string.partSixFloater_e))

            val propulsion = ArrayList<String>()
            propulsion.add(getString(R.string.partSixPropulsion_a) +
                    "\n" + getString(R.string.partSixPropulsion_b) +
                    "\n" + getString(R.string.partSixPropulsion_c) +
                    "\n" + getString(R.string.partSixPropulsion_d) +
                    "\n\n" + getString(R.string.partSixPropulsion_e))

            val directionalSystem = ArrayList<String>()
            directionalSystem.add(getString(R.string.partSixDirectionalSystem_a) +
                    "\n\n" + getString(R.string.partSixDirectionalSystem_b))

            listData[getString(R.string.partSixSubA)] = floater
            listData[getString(R.string.partSixSubB)] = propulsion
            listData[getString(R.string.partSixSubC)] = directionalSystem

            return listData.toSortedMap()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.part_one_activity)
        setUpActionBar()

        expandableListView = findViewById(R.id.expandableListView)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = CustomExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)
        }
    }

    fun setUpActionBar(){
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.essential_components)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
