package com.studio.neopanda.moncapitaine

import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PartThreeBoatActivity : AppCompatActivity() {

    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null

    val data: SortedMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val boatWhatIs = ArrayList<String>()
            boatWhatIs.add(getString(R.string.partThreeWhatIsBoat))

            val boatHistory = ArrayList<String>()
            boatHistory.add(
                getString(R.string.partThreeHistoryBoat_a) +
                        "\n\n" + getString(R.string.partThreeHistoryBoat_b) +
                        "\n\n" + getString(R.string.partThreeHistoryBoat_c) +
                        "\n\n" + getString(R.string.partThreeHistoryBoat_d) +
                        "\n\n" + getString(R.string.partThreeHistoryBoat_e) +
                        "\n\n" + getString(R.string.partThreeHistoryBoat_f)
            )

            val boatFardage = ArrayList<String>()
            boatFardage.add(getString(R.string.partThreeBoatFardage))

            val boatAllure = ArrayList<String>()
            boatAllure.add(
                getString(R.string.partThreeBoatAllure_a) +
                        "\n" + getString(R.string.partThreeBoatAllure_b) +
                        "\n" + getString(R.string.partThreeBoatAllure_c) +
                        "\n\n" + getString(R.string.partThreeBoatAllure_d) +
                        "\n\n" + getString(R.string.partThreeBoatAllure_e) +
                        "\n\n" + getString(R.string.partThreeBoatAllure_f) +
                        "\n\n" + getString(R.string.partThreeBoatAllure_g) +
                        "\n\n" + getString(R.string.partThreeBoatAllure_h) +
                        "\n\n" + getString(R.string.partThreeBoatAllure_i)
            )

            listData[getString(R.string.partThreeSubA)] = boatWhatIs
            listData[getString(R.string.partThreeSubB)] = boatHistory
            listData[getString(R.string.partThreeSubC)] = boatFardage
            listData[getString(R.string.partThreeSubD)] = boatAllure

            return listData.toSortedMap()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.part_one_activity)

        setUpActionBar()

        expandableListView = findViewById(R.id.expandableListView)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = CustomExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)
        }
    }

    fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.the_boat)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
