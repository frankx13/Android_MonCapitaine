package com.studio.neopanda.moncapitaine

import android.graphics.Color
import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PartThreeSectionTwoActivity : AppCompatActivity() {
    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null

    val data: SortedMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val introInstru = ArrayList<String>()
            introInstru.add(
                getString(R.string.partThreeSectionTwoInstrIntro)
            )

            val instrGPS = ArrayList<String>()
            instrGPS.add(
                getString(R.string.partThreeSectionTwoGPS_a)
            )

            val instrCompas = ArrayList<String>()
            instrCompas.add(
                getString(R.string.partThreesectionTwoCompas)
            )

            val instrBoussole = ArrayList<String>()
            instrBoussole.add(
                getString(R.string.partThreesectionTwoBoussole)
            )

            val instrRadar = ArrayList<String>()
            instrRadar.add(
                getString(R.string.partThreesectionTwoRadar)
            )

            val instrSondeur = ArrayList<String>()
            instrSondeur.add(
                getString(R.string.partThreesectionTwoSondeur)
            )

            val instrAnemometer = ArrayList<String>()
            instrAnemometer.add(
                getString(R.string.partThreesectionTwoAnémomètre)
            )

            val instrSpeedo = ArrayList<String>()
            instrSpeedo.add(
                getString(R.string.partThreesectionTwoLochSpeedo)
            )

            val instrAutoPilot = ArrayList<String>()
            instrAutoPilot.add(
                getString(R.string.partThreesectionTwoAutoPilot)
            )

            listData[getString(R.string.partThreeSectionTwoSubA)] = introInstru
            listData[getString(R.string.partThreeSectionTwoSubB)] = instrGPS
            listData[getString(R.string.partThreeSectionTwoSubC)] = instrCompas
            listData[getString(R.string.partThreeSectionTwoSubD)] = instrBoussole
            listData[getString(R.string.partThreeSectionTwoSubE)] = instrRadar

            listData[getString(R.string.partThreeSectionTwoSubF)] = instrSondeur

            listData[getString(R.string.partThreeSectionTwoSubG)] = instrAnemometer

            listData[getString(R.string.partThreeSectionTwoSubH)] = instrSpeedo

            listData[getString(R.string.partThreeSectionTwoSubI)] = instrAutoPilot

            return listData.toSortedMap()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.part_one_activity)

        setUpActionBar()

        expandableListView = findViewById(R.id.expandableListView)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = CustomExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)
        }
    }

    fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.navigation_instruments)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
