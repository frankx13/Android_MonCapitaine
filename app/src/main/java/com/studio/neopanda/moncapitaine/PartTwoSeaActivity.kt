package com.studio.neopanda.moncapitaine

import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PartTwoSeaActivity : AppCompatActivity() {

    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null

    val data: SortedMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val seaWhatIs = ArrayList<String>()
            seaWhatIs.add(getString(R.string.partTwoWhatIsSea))

            val seaMovements = ArrayList<String>()
            seaMovements.add(getString(R.string.partTwoSeaMovementsA))
            seaMovements.add(getString(R.string.partTwoSeaMovementsB))
            seaMovements.add(getString(R.string.partTwoSeaMovementsC))

            val seaSalinity = ArrayList<String>()
            seaSalinity.add(getString(R.string.partTwoSeaSalinity))

            listData[getString(R.string.partTwoSubA)] = seaWhatIs
            listData[getString(R.string.partTwoSubB)] = seaMovements
            listData[getString(R.string.partTwoSubC)] = seaSalinity

            return listData.toSortedMap()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.part_one_activity)
        setUpActionBar()

        expandableListView = findViewById(R.id.expandableListView)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = CustomExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)
        }
    }

    fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.the_sea)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
