package com.studio.neopanda.moncapitaine

import android.os.Bundle
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PartTwoSectionTwoActivity : AppCompatActivity() {

    internal var expandableListView: ExpandableListView? = null
    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null

    val data: SortedMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val introductionVHF = ArrayList<String>()
            introductionVHF.add(
                getString(R.string.partTwoSectionTwo_VHF_a)
            )

            val makeCall = ArrayList<String>()
            makeCall.add(
                getString(R.string.partTwoSectionTwoMakingCall_a) +
                        "\n\n" + getString(R.string.partTwoSectionTwoMakingCall_b) +
                        "\n" + getString(R.string.partTwoSectionTwoMakingCall_c) +
                        "\n" + getString(R.string.partTwoSectionTwoMakingCall_d)
            )

            val makeEmergenceCall = ArrayList<String>()
            makeEmergenceCall.add(
                getString(R.string.partTwoSectionTwoEmergencyCall_a) +
                        "\n\n" + getString(R.string.partTwoSectionTwoEmergencyCall_b) +
                        "\n" + getString(R.string.partTwoSectionTwoEmergencyCall_c) +
                        "\n" + getString(R.string.partTwoSectionTwoEmergencyCall_d) +
                        "\n" + getString(R.string.partTwoSectionTwoEmergencyCall_e) +
                        "\n" + getString(R.string.partTwoSectionTwoEmergencyCall_f)
            )

            val makeRadioCheck = ArrayList<String>()
            makeRadioCheck.add(
                getString(R.string.partTwoSectionTwoRadioCheck_a) +
                        "\n\n" + getString(R.string.partTwoSectionTwoRadioCheck_b) +
                        "\n" + getString(R.string.partTwoSectionTwoRadioCheck_c)
            )

            val importantChannels = ArrayList<String>()
            importantChannels.add(
                getString(R.string.partTwoSectionTwoImportantChannels_a) +
                        "\n\n" + getString(R.string.partTwoSectionTwoImportantChannels_b) +
                        "\n" + getString(R.string.partTwoSectionTwoImportantChannels_c) +
                        "\n" + getString(R.string.partTwoSectionTwoImportantChannels_d) +
                        "\n" + getString(R.string.partTwoSectionTwoImportantChannels_e)
            )

            listData[getString(R.string.partTwoSectionTwoSubA)] = introductionVHF
            listData[getString(R.string.partTwoSectionTwoSubB)] = makeCall
            listData[getString(R.string.partTwoSectionTwoSubC)] = makeEmergenceCall
            listData[getString(R.string.partTwoSectionTwoSubD)] = makeRadioCheck
            listData[getString(R.string.partTwoSectionTwoSubE)] = importantChannels

            return listData.toSortedMap()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.part_one_activity)

        setUpActionBar()

        expandableListView = findViewById(R.id.expandableListView)
        if (expandableListView != null) {
            val listData = data
            titleList = ArrayList(listData.keys)
            adapter = CustomExpandableListAdapter(this, titleList as ArrayList<String>, listData)
            expandableListView!!.setAdapter(adapter)
        }
    }

    fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.use_VHS)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
