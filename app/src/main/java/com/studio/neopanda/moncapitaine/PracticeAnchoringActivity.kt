package com.studio.neopanda.moncapitaine

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_practice_anchoring.*

class PracticeAnchoringActivity : AppCompatActivity() {

    private var isSchemaForwardDisplayed: Boolean = false
    private var isSchemaBackwardDisplayed: Boolean = false
    private var isExplanationForwardDisplayed: Boolean = false
    private var isExplanationBackwardDisplayed: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practice_anchoring)

        setUpActionBar()
        controlDisplaySchemaForward()
        controlDisplaySchemaBackward()
        controlDisplayExplanationForward()
        controlDisplayExplanationBackward()
    }

    fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.anchor_in_port_manoeuver_title)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun controlDisplaySchemaForward() {
        btn_display_schema_port_forward.setOnClickListener {
            isSchemaForwardDisplayed = !isSchemaForwardDisplayed
            if (isSchemaForwardDisplayed) {
                btn_display_schema_port_forward.text =
                    resources.getString(R.string.hide_schema_forward_port)
                schema_practice_port_forward.visibility = View.VISIBLE
            } else {
                btn_display_schema_port_forward.text =
                    resources.getString(R.string.display_schema_forward_port)
                schema_practice_port_forward.visibility = View.GONE
            }
        }
    }

    private fun controlDisplaySchemaBackward() {
        btn_display_schema_port_backward.setOnClickListener {
            isSchemaBackwardDisplayed = !isSchemaBackwardDisplayed
            if (isSchemaBackwardDisplayed) {
                btn_display_schema_port_backward.text =
                    resources.getString(R.string.hide_schema_backward_port)
                schema_practice_port_backward.visibility = View.VISIBLE
            } else {
                btn_display_schema_port_backward.text =
                    resources.getString(R.string.display_schema_backward_port)
                schema_practice_port_backward.visibility = View.GONE
            }
        }
    }


    private fun controlDisplayExplanationForward() {
        btn_display_explanation_forward_port.setOnClickListener {
            isExplanationForwardDisplayed = !isExplanationForwardDisplayed
            if (isExplanationForwardDisplayed) {
                btn_display_explanation_forward_port.text =
                    resources.getString(R.string.hide_explanation_forward)
                port_forward_explanation.visibility = View.VISIBLE
            } else {
                btn_display_explanation_forward_port.text =
                    resources.getString(R.string.display_explanation_forward)
                port_forward_explanation.visibility = View.GONE
            }
        }
    }

    private fun controlDisplayExplanationBackward() {
        btn_display_explanation_backward_port.setOnClickListener {
            isExplanationBackwardDisplayed = !isExplanationBackwardDisplayed
            if (isExplanationBackwardDisplayed) {
                btn_display_explanation_backward_port.text =
                    resources.getString(R.string.hide_explanation_backward)
                port_backward_explanation.visibility = View.VISIBLE
            } else {
                btn_display_explanation_backward_port.text =
                    resources.getString(R.string.display_explanation_backward)
                port_backward_explanation.visibility = View.GONE
            }
        }
    }
}
