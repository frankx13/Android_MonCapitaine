package com.studio.neopanda.moncapitaine

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_practice_backward.*
import kotlinx.android.synthetic.main.activity_practice_mooring.btn_display_explanation
import kotlinx.android.synthetic.main.activity_practice_mooring.mooring_text_explanation

class PracticeBackwardActivity : AppCompatActivity() {

    private var isSchemaOneDisplayed: Boolean = false
    private var isSchemaTwoDisplayed: Boolean = false
    private var isExplanationDisplayed: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practice_backward)
        setUpActionBar()
        controlDisplaySchemaOne()
        controlDisplaySchemaTwo()
        controlDisplayExplanation()
    }

    fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.backward_text_manoeuver)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun controlDisplaySchemaOne() {
        btn_display_schema_backward_one.setOnClickListener {
            isSchemaOneDisplayed = !isSchemaOneDisplayed
            if (isSchemaOneDisplayed) {
                btn_display_schema_backward_one.text =
                    resources.getString(R.string.hide_schema_number_one)
                schema_practice_backward_one.visibility = View.VISIBLE
            } else {
                btn_display_schema_backward_one.text =
                    resources.getString(R.string.display_schema_number_one)
                schema_practice_backward_one.visibility = View.GONE
            }
        }
    }

    private fun controlDisplaySchemaTwo() {
        btn_display_schema_backward_two.setOnClickListener {
            isSchemaTwoDisplayed = !isSchemaTwoDisplayed
            if (isSchemaTwoDisplayed) {
                btn_display_schema_backward_two.text =
                    resources.getString(R.string.hide_schema_number_two)
                schema_practice_backward_two.visibility = View.VISIBLE
            } else {
                btn_display_schema_backward_two.text =
                    resources.getString(R.string.display_schema_number_two)
                schema_practice_backward_two.visibility = View.GONE
            }
        }
    }

    private fun controlDisplayExplanation() {
        btn_display_explanation.setOnClickListener {
            isExplanationDisplayed = !isExplanationDisplayed
            if (isExplanationDisplayed) {
                btn_display_explanation.text = resources.getString(R.string.hide_explanation)
                mooring_text_explanation.visibility = View.VISIBLE
            } else {
                btn_display_explanation.text = resources.getString(R.string.display_explanation)
                mooring_text_explanation.visibility = View.GONE
            }
        }
    }
}
