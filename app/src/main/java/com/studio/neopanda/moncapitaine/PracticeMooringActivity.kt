package com.studio.neopanda.moncapitaine

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_practice_mooring.*

class PracticeMooringActivity : AppCompatActivity() {

    private var isSchemaDisplayed: Boolean = false
    private var isExplanationDisplayed: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practice_mooring)

        setUpActionBar()
        controlDisplaySchema()
        controlDisplayExplanation()
    }

    fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = "Manoeuvre : Se mettre au mouillage"
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun controlDisplaySchema() {
        btn_display_schema.setOnClickListener {
            isSchemaDisplayed = !isSchemaDisplayed
            if (isSchemaDisplayed) {
                btn_display_schema.text = resources.getString(R.string.hide_schema)
                schema_practice_mooring_rule.visibility = View.VISIBLE
            } else {
                btn_display_schema.text = resources.getString(R.string.display_schema)
                schema_practice_mooring_rule.visibility = View.GONE
            }
        }
    }


    private fun controlDisplayExplanation() {
        btn_display_explanation.setOnClickListener {
            isExplanationDisplayed = !isExplanationDisplayed
            if (isExplanationDisplayed) {
                btn_display_explanation.text = resources.getString(R.string.hide_explanation)
                mooring_text_explanation.visibility = View.VISIBLE
            } else {
                btn_display_explanation.text = resources.getString(R.string.display_explanation)
                mooring_text_explanation.visibility = View.GONE
            }
        }
    }
}
