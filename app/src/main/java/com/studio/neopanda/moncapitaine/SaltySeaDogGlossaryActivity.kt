package com.studio.neopanda.moncapitaine

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_salty_sea_dog_glossary.*


class SaltySeaDogGlossaryActivity : AppCompatActivity() {

    private var listExpressions = ArrayList<String>()
    private var listDefinitions = ArrayList<String>()
    private var listObjects = ArrayList<Expression>()
    private var numberOfExpressions: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_salty_sea_dog_glossary)

        setUpActionBar()
        fillExpressionsList()
        fillDefinitionsList()
        buildingExpressionsObjects()
        sortingObjects()
    }

    private fun fillExpressionsList() {
        listExpressions.add(resources.getString(R.string.glossary_1))
        listExpressions.add(resources.getString(R.string.glossary_2))
        listExpressions.add(resources.getString(R.string.glossary_3))
        listExpressions.add(resources.getString(R.string.glossary_4))
        listExpressions.add(resources.getString(R.string.glossary_5))
        listExpressions.add(resources.getString(R.string.glossary_6))
        listExpressions.add(resources.getString(R.string.glossary_7))
        listExpressions.add(resources.getString(R.string.glossary_8))
        listExpressions.add(resources.getString(R.string.glossary_9))
        listExpressions.add(resources.getString(R.string.glossary_10))
        listExpressions.add(resources.getString(R.string.glossary_11))
        listExpressions.add(resources.getString(R.string.glossary_12))
        listExpressions.add(resources.getString(R.string.glossary_13))
        listExpressions.add(resources.getString(R.string.glossary_14))
        listExpressions.add(resources.getString(R.string.glossary_15))
        listExpressions.add(resources.getString(R.string.glossary_16))
        listExpressions.add(resources.getString(R.string.glossary_17))

        numberOfExpressions = listExpressions.size
    }

    private fun fillDefinitionsList() {
        listDefinitions.add(resources.getString(R.string.glossary_def_1))
        listDefinitions.add(resources.getString(R.string.glossary_def_2))
        listDefinitions.add(resources.getString(R.string.glossary_def_3))
        listDefinitions.add(resources.getString(R.string.glossary_def_4))
        listDefinitions.add(resources.getString(R.string.glossary_def_5))
        listDefinitions.add(resources.getString(R.string.glossary_def_6))
        listDefinitions.add(resources.getString(R.string.glossary_def_7))
        listDefinitions.add(resources.getString(R.string.glossary_def_8))
        listDefinitions.add(resources.getString(R.string.glossary_def_9))
        listDefinitions.add(resources.getString(R.string.glossary_def_10))
        listDefinitions.add(resources.getString(R.string.glossary_def_11))
        listDefinitions.add(resources.getString(R.string.glossary_def_12))
        listDefinitions.add(resources.getString(R.string.glossary_def_13))
        listDefinitions.add(resources.getString(R.string.glossary_def_14))
        listDefinitions.add(resources.getString(R.string.glossary_def_15))
        listDefinitions.add(resources.getString(R.string.glossary_def_16))
        listDefinitions.add(resources.getString(R.string.glossary_def_17))
    }

    private fun buildingExpressionsObjects() {
        for (i in 0 until numberOfExpressions) {
            listObjects.add(Expression(listExpressions[i], listDefinitions[i], i))
        }
    }

    private fun sortingObjects() {
        val sortedList = listObjects.sortedWith(compareBy { it.name })

        configureRecyclerView(sortedList)
    }

    private fun configureRecyclerView(listExp: List<Expression>) {
        val recyclerAdapter = SaltySeaDogGlossaryAdapter(this, listExp)
        glossary_recyclerview.adapter = recyclerAdapter
        glossary_recyclerview.layoutManager = LinearLayoutManager(this)
    }

    fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.glossary_title)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
