package com.studio.neopanda.moncapitaine

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SaltySeaDogGlossaryAdapter : RecyclerView.Adapter<SaltySeaDogGlossaryAdapter.MyViewHolder> {

    private var ctx: Context
    private var listExpressions: List<Expression>

    constructor(ctx: Context, listExpressions: List<Expression>) {
        this.ctx = ctx
        this.listExpressions = listExpressions
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v: View = LayoutInflater.from(ctx)
            .inflate(R.layout.glossary_item, parent, false)
        return MyViewHolder(v)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.expressionTV.text = listExpressions[position].name
        holder.definitionTV.text = listExpressions[position].explanation
    }

    override fun getItemCount(): Int {
        return listExpressions.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var expressionTV: TextView = itemView.findViewById(R.id.glossary_item_expression)
        var definitionTV: TextView = itemView.findViewById(R.id.glossary_item_definition)
    }
}