package com.studio.neopanda.moncapitaine

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class SectionFiveActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_section_five)

        setUpActionBar()
    }

    fun setUpActionBar(){
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string._5_credits)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
