package com.studio.neopanda.moncapitaine

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import butterknife.ButterKnife

class SectionOneActivity : AppCompatActivity() {

    @BindView(R.id.part_one_wind_btn)
    lateinit var partOneWind: Button
    @BindView(R.id.part_two_sea_btn)
    lateinit var partTwoSea: Button
    @BindView(R.id.part_three_boat_btn)
    lateinit var partThreeBoat: Button
    @BindView(R.id.part_four_floatability_btn)
    lateinit var partFourFloat: Button
    @BindView(R.id.part_five_speed_btn)
    lateinit var partFiveSpeed: Button
    @BindView(R.id.part_six_composants_btn)
    lateinit var partSixComposants: Button
    @BindView(R.id.part_seven_composants_btn)
    lateinit var partSevenBoatingLicense: Button
    @BindView(R.id.part_eight_priorities_btn)
    lateinit var partEightPriorities: Button
    @BindView(R.id.part_nine_meteo_btn)
    lateinit var partNineMeteo: Button
    @BindView(R.id.part_one_step_IV)
    lateinit var partOneIV: ImageView
    @BindView(R.id.part_two_step_IV)
    lateinit var partTwoIV: ImageView
    @BindView(R.id.part_three_step_IV)
    lateinit var partThreeIV: ImageView
    @BindView(R.id.part_four_step_IV)
    lateinit var partFourIV: ImageView
    @BindView(R.id.part_five_step_IV)
    lateinit var partFiveIV: ImageView
    @BindView(R.id.part_six_step_IV)
    lateinit var partSixIV: ImageView
    @BindView(R.id.part_seven_step_IV)
    lateinit var partSevenIV: ImageView
    @BindView(R.id.part_eight_step_IV)
    lateinit var partEightIV: ImageView
    @BindView(R.id.part_nine_step_IV)
    lateinit var partNineIV: ImageView

    private var isChapterFinishedOne: Boolean = false
    private var isChapterFinishedTwo: Boolean = false
    private var isChapterFinishedThree: Boolean = false
    private var isChapterFinishedFour: Boolean = false
    private var isChapterFinishedFive: Boolean = false
    private var isChapterFinishedSix: Boolean = false
    private var isChapterFinishedSeven: Boolean = false
    private var isChapterFinishedEight: Boolean = false
    private var isChapterFinishedNine: Boolean = false
    private var partNumber = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_section_one)

        ButterKnife.bind(this)
        onClickSpecificChapter()
        onClickFinishedImage()
        setUpActionBar()
    }

    private fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.basicTheory)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun onClickSpecificChapter() {
        partOneWind.setOnClickListener {
            partNumber = 1
            val intent = Intent(this, PartOneWindActivity::class.java)
            intent.putExtra("partNumber", partNumber)
            startActivity(intent)
        }
        partTwoSea.setOnClickListener {
            partNumber = 2
            val intent = Intent(this, PartTwoSeaActivity::class.java)
            intent.putExtra("partNumber", partNumber)
            startActivity(intent)
        }
        partThreeBoat.setOnClickListener {
            partNumber = 3
            val intent = Intent(this, PartThreeBoatActivity::class.java)
            intent.putExtra("partNumber", partNumber)
            startActivity(intent)
        }
        partFourFloat.setOnClickListener {
            partNumber = 4
            val intent = Intent(this, PartFourFloatActivity::class.java)
            intent.putExtra("partNumber", partNumber)
            startActivity(intent)
        }
        partFiveSpeed.setOnClickListener {
            partNumber = 5
            val intent = Intent(this, PartFiveSpeedActivity::class.java)
            intent.putExtra("partNumber", partNumber)
            startActivity(intent)
        }
        partSixComposants.setOnClickListener {
            partNumber = 6
            val intent = Intent(this, PartSixComposantsActivity::class.java)
            intent.putExtra("partNumber", partNumber)
            startActivity(intent)
        }
        partSevenBoatingLicense.setOnClickListener {
            partNumber = 7
            val intent = Intent(this, PartSevenBoatingLicenseActivity::class.java)
            intent.putExtra("partNumber", partNumber)
            startActivity(intent)
        }
        partEightPriorities.setOnClickListener {
            partNumber = 8
            val intent = Intent(this, PartEightPrioritiesActivity::class.java)
            intent.putExtra("partNumber", partNumber)
            startActivity(intent)
        }
        partNineMeteo.setOnClickListener {
            partNumber = 9
            val intent = Intent(this, PartNineMeteoActivity::class.java)
            intent.putExtra("partNumber", partNumber)
            startActivity(intent)
        }
    }

    private fun onClickFinishedImage() {
        partOneIV.setOnClickListener {
            isChapterFinishedOne = if (isChapterFinishedOne) {
                partOneIV.setBackgroundResource(R.drawable.book_item)
                false
            } else {
                partOneIV.setBackgroundResource(R.drawable.book_item_finished)
                true
            }
            Toast.makeText(this, "You finished the Chapter one !", Toast.LENGTH_SHORT).show()
        }
        partTwoIV.setOnClickListener {
            isChapterFinishedTwo = if (isChapterFinishedTwo) {
                partTwoIV.setBackgroundResource(R.drawable.book_item)
                false
            } else {
                partTwoIV.setBackgroundResource(R.drawable.book_item_finished)
                true
            }
            Toast.makeText(this, "You finished the Chapter two !", Toast.LENGTH_SHORT).show()
        }
        partThreeIV.setOnClickListener {
            isChapterFinishedThree = if (isChapterFinishedThree) {
                partThreeIV.setBackgroundResource(R.drawable.book_item)
                false
            } else {
                partThreeIV.setBackgroundResource(R.drawable.book_item_finished)
                true
            }
            Toast.makeText(this, "You finished the Chapter three !", Toast.LENGTH_SHORT).show()
        }
        partFourIV.setOnClickListener {
            isChapterFinishedFour = if (isChapterFinishedFour) {
                partFourIV.setBackgroundResource(R.drawable.book_item)
                false
            } else {
                partFourIV.setBackgroundResource(R.drawable.book_item_finished)
                true
            }
            Toast.makeText(this, "You finished the Chapter four !", Toast.LENGTH_SHORT).show()
        }
        partFiveIV.setOnClickListener {
            isChapterFinishedFive = if (isChapterFinishedNine) {
                partFiveIV.setBackgroundResource(R.drawable.book_item)
                false
            } else {
                partFiveIV.setBackgroundResource(R.drawable.book_item_finished)
                true
            }
            Toast.makeText(this, "You finished the Chapter five !", Toast.LENGTH_SHORT).show()
        }
        partSixIV.setOnClickListener {
            isChapterFinishedSix = if (isChapterFinishedSix) {
                partSixIV.setBackgroundResource(R.drawable.book_item)
                false
            } else {
                partSixIV.setBackgroundResource(R.drawable.book_item_finished)
                true
            }
            Toast.makeText(this, "You finished the Chapter six !", Toast.LENGTH_SHORT).show()
        }
        partSevenIV.setOnClickListener {
            isChapterFinishedSeven = if (isChapterFinishedSeven) {
                partSevenIV.setBackgroundResource(R.drawable.book_item)
                false
            } else {
                partSevenIV.setBackgroundResource(R.drawable.book_item_finished)
                true
            }
            Toast.makeText(this, "You finished the Chapter seven !", Toast.LENGTH_SHORT).show()
        }
        partEightIV.setOnClickListener {
            isChapterFinishedEight = if (isChapterFinishedEight) {
                partEightIV.setBackgroundResource(R.drawable.book_item)
                false
            } else {
                partEightIV.setBackgroundResource(R.drawable.book_item_finished)
                true
            }
            Toast.makeText(this, "You finished the Chapter eight !", Toast.LENGTH_SHORT).show()
        }
        partNineIV.setOnClickListener {
            isChapterFinishedNine = if (isChapterFinishedNine) {
                partNineIV.setBackgroundResource(R.drawable.book_item)
                false
            } else {
                partNineIV.setBackgroundResource(R.drawable.book_item_finished)
                true
            }
            Toast.makeText(this, "You finished the Chapter nine !", Toast.LENGTH_SHORT).show()
        }
    }
}