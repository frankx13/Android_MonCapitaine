package com.studio.neopanda.moncapitaine

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import butterknife.ButterKnife


class SectionThreeActivity : AppCompatActivity() {

    @BindView(R.id.image_wheel_moving)
    lateinit var imageWheel: ImageView
    @BindView(R.id.b1)
    lateinit var backwardManeuver: Button
    @BindView(R.id.b2)
    lateinit var ninetyDegreesManeuver: Button
    @BindView(R.id.b3)
    lateinit var parkInPortManeuver: Button
    @BindView(R.id.b4)
    lateinit var parkInWildManeuver: Button

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_section_three)

        ButterKnife.bind(this)
        setUpActionBar()

        val listener = View.OnTouchListener(function = { view, motionEvent ->

            if (motionEvent.action == MotionEvent.ACTION_MOVE) {
                view.y = motionEvent.rawY - view.height / 2
                view.x = motionEvent.rawX - view.width / 2
                when {
                    overlappingBackwards() -> overlappingWithBack()
                    overlappingNinetyDegrees() -> overlappingWithNinetees()
                    overlappingMooringManeuver() -> overlappingWithMooring()
                    overlappingAnchoringManeuver() -> overlappingWithAnchoring()
                    else -> notOverlapping()
                }
            }
            true
        })

        imageWheel.setOnTouchListener(listener)
    }

    private fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.basic_practice)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun overlappingBackwards(): Boolean {
        val firstPosition = IntArray(2)
        val secondPosition = IntArray(2)

        imageWheel.getLocationOnScreen(firstPosition)
        backwardManeuver.getLocationOnScreen(secondPosition)

        // Rect constructor parameters: left, top, right, bottom
        val rectFirstView = Rect(
            firstPosition[0],
            firstPosition[1],
            firstPosition[0] + imageWheel.measuredWidth,
            firstPosition[1] + imageWheel.measuredHeight
        )
        val rectSecondView = Rect(
            secondPosition[0],
            secondPosition[1],
            secondPosition[0] + backwardManeuver.measuredWidth,
            secondPosition[1] + backwardManeuver.measuredHeight
        )

        return rectFirstView.intersect(rectSecondView)
    }

    private fun overlappingNinetyDegrees(): Boolean {
        val firstPosition = IntArray(2)
        val secondPosition = IntArray(2)

        imageWheel.getLocationOnScreen(firstPosition)
        ninetyDegreesManeuver.getLocationOnScreen(secondPosition)

        // Rect constructor parameters: left, top, right, bottom
        val rectFirstView = Rect(
            firstPosition[0],
            firstPosition[1],
            firstPosition[0] + imageWheel.measuredWidth,
            firstPosition[1] + imageWheel.measuredHeight
        )
        val rectSecondView = Rect(
            secondPosition[0],
            secondPosition[1],
            secondPosition[0] + ninetyDegreesManeuver.measuredWidth,
            secondPosition[1] + ninetyDegreesManeuver.measuredHeight
        )

        return rectFirstView.intersect(rectSecondView)
    }

    private fun overlappingMooringManeuver(): Boolean {
        val firstPosition = IntArray(2)
        val secondPosition = IntArray(2)

        imageWheel.getLocationOnScreen(firstPosition)
        parkInPortManeuver.getLocationOnScreen(secondPosition)

        // Rect constructor parameters: left, top, right, bottom
        val rectFirstView = Rect(
            firstPosition[0],
            firstPosition[1],
            firstPosition[0] + imageWheel.measuredWidth,
            firstPosition[1] + imageWheel.measuredHeight
        )
        val rectSecondView = Rect(
            secondPosition[0],
            secondPosition[1],
            secondPosition[0] + parkInPortManeuver.measuredWidth,
            secondPosition[1] + parkInPortManeuver.measuredHeight
        )

        return rectFirstView.intersect(rectSecondView)
    }

    private fun overlappingAnchoringManeuver(): Boolean {
        val firstPosition = IntArray(2)
        val secondPosition = IntArray(2)

        imageWheel.getLocationOnScreen(firstPosition)
        parkInWildManeuver.getLocationOnScreen(secondPosition)

        // Rect constructor parameters: left, top, right, bottom
        val rectFirstView = Rect(
            firstPosition[0],
            firstPosition[1],
            firstPosition[0] + imageWheel.measuredWidth,
            firstPosition[1] + imageWheel.measuredHeight
        )
        val rectSecondView = Rect(
            secondPosition[0],
            secondPosition[1],
            secondPosition[0] + parkInWildManeuver.measuredWidth,
            secondPosition[1] + parkInWildManeuver.measuredHeight
        )

        return rectFirstView.intersect(rectSecondView)
    }

    private fun notOverlapping() {
        backwardManeuver.background = getDrawable(R.drawable.btn_main_bg)
        ninetyDegreesManeuver.background = getDrawable(R.drawable.btn_main_bg)
        parkInPortManeuver.background = getDrawable(R.drawable.btn_main_bg)
        parkInWildManeuver.background = getDrawable(R.drawable.btn_main_bg)
        backwardManeuver.scaleY = 1.0f
        backwardManeuver.scaleX = 1.0f
        ninetyDegreesManeuver.scaleX = 1.0f
        ninetyDegreesManeuver.scaleY = 1.0f
        parkInPortManeuver.scaleX = 1.0f
        parkInPortManeuver.scaleY = 1.0f
        parkInWildManeuver.scaleY = 1.0f
        parkInWildManeuver.scaleY = 1.0f
    }

    private fun overlappingWithBack() {
        backwardManeuver.scaleY = 1.1f
        backwardManeuver.scaleX = 1.1f
        backwardManeuver.background = getDrawable(R.drawable.btn_selected_bg)
        backwardManeuver.setOnClickListener {
            val intent = Intent(this, PracticeBackwardActivity::class.java)
            startActivity(intent)
        }
    }

    private fun overlappingWithNinetees() {
        ninetyDegreesManeuver.scaleY = 1.1f
        ninetyDegreesManeuver.scaleX = 1.1f
        ninetyDegreesManeuver.background = getDrawable(R.drawable.btn_selected_bg)
        ninetyDegreesManeuver.setOnClickListener {
            Toast.makeText(this, resources.getString(R.string.available_next_release),
                Toast.LENGTH_LONG).show()
        }
    }

    private fun overlappingWithMooring() {
        parkInPortManeuver.scaleY = 1.1f
        parkInPortManeuver.scaleX = 1.1f
        parkInPortManeuver.background = getDrawable(R.drawable.btn_selected_bg)
        parkInPortManeuver.setOnClickListener {
            val intent = Intent(this, PracticeAnchoringActivity::class.java)
            startActivity(intent)
        }
    }

    private fun overlappingWithAnchoring() {
        parkInWildManeuver.scaleY = 1.1f
        parkInWildManeuver.scaleX = 1.1f
        parkInWildManeuver.background = getDrawable(R.drawable.btn_selected_bg)
        parkInWildManeuver.setOnClickListener {
            val intent = Intent(this, PracticeMooringActivity::class.java)
            startActivity(intent)
        }
    }
}
