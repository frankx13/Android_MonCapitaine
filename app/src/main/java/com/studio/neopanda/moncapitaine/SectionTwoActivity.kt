package com.studio.neopanda.moncapitaine

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import butterknife.ButterKnife

class SectionTwoActivity : AppCompatActivity() {

    @BindView(R.id.part_one_section_2_direct_boat)
    lateinit var directionBoat: Button
    @BindView(R.id.part_two_section_2_radio_vhf)
    lateinit var useRadioVHF: Button
    @BindView(R.id.part_three_section_2_nav_instruments)
    lateinit var navInstruments: Button
    @BindView(R.id.part_four_section_2_functionnement_sails)
    lateinit var partFour: Button
    @BindView(R.id.part_five_section_2_use_sails)
    lateinit var partFive : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_section_two)

        ButterKnife.bind(this)
        setUpActionBar()
        directionBoat.setOnClickListener {
            val intent = Intent(this, PartOneSectionTwoActivity::class.java)
            startActivity(intent)
        }
        useRadioVHF.setOnClickListener {
            val intent = Intent(this, PartTwoSectionTwoActivity::class.java)
            startActivity(intent)
        }
        navInstruments.setOnClickListener {
            val intent = Intent(this, PartThreeSectionTwoActivity::class.java)
            startActivity(intent)
        }
        partFour.setOnClickListener {
            Toast.makeText(this, resources.getString(R.string.available_next_release),
                Toast.LENGTH_LONG).show()
        }
        partFive.setOnClickListener {
            Toast.makeText(this, resources.getString(R.string.available_next_release),
                Toast.LENGTH_LONG).show()
        }
    }

    fun setUpActionBar() {
        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.advancedTheory)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}